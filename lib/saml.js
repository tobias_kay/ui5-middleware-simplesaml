const getSAMLProxy = require('saml-solver').getSAMLSolverProxy;
const homeDir = require("os").homedir();

/**
 * Custom UI5 Server middleware example
 *
 * @param {Object} parameters Parameters
 * @param {Object} parameters.resources Resource collections
 * @param {module:@ui5/fs.AbstractReader} parameters.resources.all Reader or Collection to read resources of the
 *                                        root project and its dependencies
 * @param {module:@ui5/fs.AbstractReader} parameters.resources.rootProject Reader or Collection to read resources of
 *                                        the project the server is started in
 * @param {module:@ui5/fs.AbstractReader} parameters.resources.dependencies Reader or Collection to read resources of
 *                                        the projects dependencies
 * @param {Object} parameters.options Options
 * @param {string} [parameters.options.configuration] Custom server middleware configuration if given in ui5.yaml
 * @returns {function} Middleware function to use
 */

module.exports = function ({ options: { configuration } }) {

    const { baseUrl, initPath } = configuration;
    let { user = undefined, password = undefined } = configuration;
    const cfg_path = homeDir + (process.platform === "win32" ? "/" : "/.") + 'saml_auth.json';
    try {
        const cfg = require(cfg_path);
        if (cfg.user !== undefined)
            user = cfg.user;
        if (cfg.password !== undefined)
            password = cfg.password;
        if (cfg.user === undefined || cfg.password === undefined)
            console.warn(`no user config found in ${cfg_path}...`);
    } catch (e) {
        console.warn(`no user config found in ${cfg_path}...`);
    }

    if (user === undefined || password === undefined)
        throw Error("Invalid configuration for saml proxy middleware: user or password not set!");

    const optionals = {};
    ["urlBlacklist", "manualInteractions", "keepaliveFreq", "pathMappings", "debug", "maxRetries"].forEach(key => {
        if (configuration[key] !== undefined)
            optionals[key] = configuration[key];
    });

    const proxy = getSAMLProxy(user, password, baseUrl, initPath, optionals);

    return (req, res) => proxy(req, res);
};
